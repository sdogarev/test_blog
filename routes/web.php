<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('welcome.index');



Route::get('/posts', 'BlogController@index')->name('blog.index');
Route::get('/posts/{post}', 'BlogController@show')->name('blog.show');

//Admin panel
/**
 * @param middleware "auth" check auth user, "admin" check is_admin
 */
Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth','admin'],
    'namespace' => 'Admin'
    ], function(){

    Route::get('/', 'DashboardController@index')->name('admin.dashboard.index');
    Route::get('/addpost', 'PostsController@create')->name('admin.posts.create');
    Route::post('/addpost/store', 'PostsController@store')->name('admin.posts.store');
    Route::get('/blog-posts', 'PostsController@index')->name('admin.posts.index');
    Route::get('/edit-post/{post}', 'PostsController@edit')->name('admin.posts.edit');
    Route::post('/edit-post/{post}/edit', 'PostsController@update')->name('admin.posts.update');
    //Admin tables users
    Route::get('/tables', 'UsersController@index')->name('admin.users.index');
    Route::post('/tables/{user}/edit', 'UsersController@edit')->name('admin.users.edit');
});





