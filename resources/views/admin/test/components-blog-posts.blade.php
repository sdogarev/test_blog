@extends('layouts.admin.app')
@section('content')
<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Components</span>
                <h3 class="page-title">Blog Posts</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <div class="row">
             @foreach($posts as $post)
              <div class="col-lg-3 col-md-6 col-sm-12 mb-4">
                <div class="card card-small card-post card-post--1">
                  <div class="card-post__image" style="background-image: url('../storage/posts/{{$post->images }}');">
                    <a href="#" class="card-post__category badge badge-pill badge-dark">{{ $post->tag }}</a>
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">
                      <a class="text-fiord-blue" href="{{ route('blog.index', ['id' => $post->id]) }}">{{ $post->title }}</a>
                    </h5>
                    <p class="card-text d-inline-block mb-3">{{ Str::substr($post->content, 0 , 40) }}...</p>
                    <span class="text-muted">{{ \Carbon\Carbon::parse($post->created_at)->format('d D Y')  }}</span>
                  </div>
                    <a class="btn btn-primary mb-1" href="{{ route('blog.show', ['id' => $post->id]) }}" style="color: white">View</a>
                   <a class="btn btn-primary" href="{{ route('admin.posts.edit', ['id' => $post->id]) }}" style="color: white">Edit</a>
                </div>
              </div>
               @endforeach
                 <nav aria-label="Page navigation example">
                     <ul class="pagination">
                         <li class="page-item">{{$posts->links()}}</li>
                     </ul>
                 </nav>
            </div>
    @endsection
