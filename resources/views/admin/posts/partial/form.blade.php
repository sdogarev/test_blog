@php
//$action = (isset($post) && !empty($post)) ? route('admin.posts.update') : route('admin.posts.store');
@endphp
<form class="add-new-post" action="{{ $action }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{--                        @if ($errors->has('post_title'))--}}
    {{--                            <p>Введите Title</p>--}}
    {{--                        @endif--}}
    <input class="form-control form-control-lg mb-3" value="{{ $post->title ?? '' }}" name="post_title"
           type="text" placeholder="Your Post Title">
    <textarea cols="152" name="post_content" id="editor-container" class="add-new-post__editor mb-1 ql-container ql-snow">
        {{ $post->content ?? '' }}
    </textarea>
    <input class="form-control form-control-lg mb-3" value="{{ $post->images ?? '' }}" name="post_images" type="file">
    <input class="form-control form-control-lg mb-3" value="{{ $post->tag ?? '' }}" name="tag" type="text" placeholder="tag" >
    <label for="example-datetime-local-input" class="col-2 col-form-label">Date and time</label>
    <div class="col-10 mb-3">
        <input name="date_time" class="form-control" value="{{ $post->created_at ?? '' }}" type="datetime-local"
               value="2011-08-19T13:45:00" id="example-datetime-local-input" >
    </div>
    <button class="btn btn-sm btn-accent ml-auto" type="submit">
        <i class="material-icons">file_copy</i> Publish</button>
</form>
