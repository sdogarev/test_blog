<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    //TableUsers
    public static function showAllAdmins(){
        return User::where('is_admin', 1)->orderBy('id', 'desc')->paginate(10);
    }

    public static function showAllUsers(){
        return User::where('is_admin', 0)->orderBy('id', 'desc')->paginate(10);
    }

    //Проверка на наличие админки.
    public static function is_admin($id){
        if(User::find($id)->is_admin == 1)
            return 0;
        else
            return 1;
    }
}
