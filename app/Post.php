<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Post extends Model
{
    public function insertPost($request){
        // Проверка на валидацию
        $validation = $request->validate([
            'post_images' => 'required',
            'post_title' => 'required|max:260',
            'post_content' => 'required|min:1',
            'tag' => 'required|max:20',
        ]);
        $this->images = $request->file('post_images')->store('', 'public');
        $this->title = $request->input('post_title');
        $this->content = $request->input('post_content');
        $this->user_posted = \Auth::user()->name;
        $this->tag = $request->input('tag');

        $this->save();
    }

    public function validationEditFormPost($request){
        // Проверка на валидацию
        $validationEdit = $request->validate([
            'post_images' => 'required',
            'post_title' => 'max:260',
            'post_content' => 'min:1',
            'tag' => 'max:20',
        ]);
    }

//    public function showAllPost(){
//       return Post::orderBy('id', 'desc')->paginate(3);
//    }

    public function findPost($id){
        return Post::find($id);
    }
}
