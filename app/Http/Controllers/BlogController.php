<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

use App\Post;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(3);
        $count = count($posts);
        return view('blog.index', [
            'posts' => $posts,
            'count' => $count,
        ]);
    }
    public function show($id)
    {
        $posts = Post::all();
        $post = Post::find($id);
        return view('blog.show', [
            'posts' => $posts,
            'post' => $post,
        ]);
    }
}
