<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;


class PostsController extends Controller
{
    /**
     * @link http://blog.local/admin/blog-posts  admin panel with all blog posts and edit
     * @return array return all posts
     */
    public function index(){
        $posts = Post::orderBy('id', 'desc')->paginate(8);

        return view('admin.posts.index', [
            'posts' => $posts,
        ]);
    }

    /**
     * @link http://blog.local/posts all blog posts
     * @return array post
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('blog.show', compact('post'));
    }

    /**
     * @link http://blog.local/admin/edit-post/id admin panel edit blog post
     * @return array post
     */
    public function edit($id){
        $post = Post::find($id);
        return view('admin.posts.edit', [
            'post' => $post,
            'action' => route('admin.posts.update', ['id' => $post->id]),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
//        $post = Post::all();
        return view('admin.posts.create', [
//            'post' => $post,
            'action' => route('admin.posts.store'),
        ]);
    }

    /**
     * @param Request $request all element inputs
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Если проверка прошла успешно, тогда $row сохраняем в базу данных.
        $post = new Post;

        $post->insertPost($request);

        $post->images = $request->file('post_images')->store('', 'public');
        $post->title = $request->input('post_title');
        $post->content = $request->input('post_content');
        $post->user_posted = \Auth::user()->name;
        $post->tag = $request->input('tag');

        $post->save();

        return redirect('/posts'); // Redirect на страницу BLOG {{url(/posts)}}
    }

    /**
     * @param Request $request element input
     * @param $id id element input
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id){
        $post = new Post;
        $post = $post->findPost($id);


        $post->validationEditFormPost($request);

        $post->images = $request->file('post_images')->store('', 'public');
        $post->title = $request->input('post_title');
        $post->content = $request->input('post_content');
        $post->user_posted = \Auth::user()->name;
        $post->tag = $request->input('tag');

        $post->save();


        return redirect('/posts');
    }

    ///
}
