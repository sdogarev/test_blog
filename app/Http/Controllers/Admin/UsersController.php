<?php

namespace App\Http\Controllers\Admin;

use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::showAllUsers();
        $admins = User::showAllAdmins();
        return view('admin.users.index', [
            'users'=> $users,
            'admins'=>$admins,
        ]);
    }

    public function edit($id){
        $user = User::find($id);

        $user->is_admin = User::is_admin($id);

        $user->save();


        return redirect(route('admin.users.index'));
    }
}
