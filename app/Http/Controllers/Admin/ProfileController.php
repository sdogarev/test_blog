<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::get();
        $users = Post::get();

//        $admin = new User; //test

        $count_posts = count($posts); // Количество постов
        $count_users = count($users); // Количество users
        return view('admin.index', [
            'count_posts' => $count_posts,
            'count_users' => $count_users,
//            'admin' => $admin,
        ]);
    }
}
