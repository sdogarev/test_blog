<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    public function index()
    {
        $posts = Post::get();
        $users = User::get();

        $count_posts = count($posts); // Количество постов
        $count_users = count($users); // Количество users
        return view('admin.dashboard.index', [
            'count_posts' => $count_posts,
            'count_users' => $count_users,
        ]);
    }
}
